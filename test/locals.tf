# root/locals.tf

locals {
  users = {
    tf-mosar-feature-flags = {
      path = "/terraform/"
      name = "tf_mosar_feature_flags"
      policy = {
        name        = "tf_mosar_feature_flags_policy"
        description = "Policy for tf role managing the feature flags service (in ECS)"
        policy      = file("${path.root}/user_policies/tf_mosar_feature_flags_policy.json")
      }
    }
    tf-mosar-aurora-cluster = {
      path = "/terraform/"
      name = "tf_mosar_aurora_cluster"
      policy = {
        name        = "tf_mosar_aurora_cluster_policy"
        description = "Policy for tf role managing the aurora rds db cluster"
        policy      = file("${path.root}/user_policies/tf_mosar_aurora_cluster_policy.json")
      }
    }
    tf-mosar-key-pairs = {
      path = "/terraform/"
      name = "tf_mosar_key_pairs"
      policy = {
        name        = "tf_mosar_key_pairs_policy"
        description = "Policy for tf role managing the key-pairs"
        policy      = file("${path.root}/user_policies/tf_mosar_key_pair_policy.json")
      }
    }
    tf-mosar-jump-servers = {
      path = "/terraform/"
      name = "tf_mosar_jump_servers"
      policy = {
        name        = "tf_mosar_jump_servers_policy"
        description = "Policy for tf role managing the jump servers"
        policy      = file("${path.root}/user_policies/tf_mosar_jump_servers_policy.json")
      }
    }
    tf-mosar-nat-instance = {
      path = "/terraform/"
      name = "tf_mosar_nat_instance"
      policy = {
        name        = "tf_mosar_nat_instance_policy"
        description = "Policy for tf role managing the nat instance"
        policy      = file("${path.root}/user_policies/tf_mosar_nat_instance_policy.json")
      }
    }
    tf-mosar-jenkins = {
      path = "/terraform/"
      name = "tf_mosar_jenkins"
      policy = {
        name        = "tf_mosar_jenkins_policy"
        description = "Policy for tf role managing the jenkins server"
        policy      = file("${path.root}/user_policies/tf_mosar_jenkins_policy.json")
      }
    }
    tf-mosar-vpc = {
      path = "/terraform/"
      name = "tf_mosar_vpc"
      policy = {
        name        = "tf_mosar_vpc_policy"
        description = "Policy for tf role managing the vpc resources"
        policy      = file("${path.root}/user_policies/tf_mosar_vpc_policy.json")
      }
    }
    tf-mosar-ecs-cluster = {
      path = "/terraform/"
      name = "tf_mosar_ecs_cluster"
      policy = {
        name        = "tf_mosar_ecs_cluster_policy"
        description = "Policy for tf role managing the ecs cluster resources"
        policy      = file("${path.root}/user_policies/tf_mosar_ecs_cluster_policy.json")
      }
    }
    tf-jenkins = {
      path = "/terraform/"
      name = "tf_jenkins"
      policy = {
        name        = "tf_jenkins_policy"
        description = "Policy for tf role managing jenkins in ecs cluster resources"
        policy      = file("${path.root}/user_policies/tf_jenkins_policy.json")
      }
    }
    tf-ecr = {
      path = "/terraform/"
      name = "tf_ecr"
      policy = {
        name        = "tf_ecr_policy"
        description = "Policy for tf role managing ecr resources"
        policy      = file("${path.root}/user_policies/tf_ecr_policy.json")
      }
    }
    tf-vpc-endpoint = {
      path = "/terraform/"
      name = "tf_vpc_endpoint"
      policy = {
        name        = "tf_vpc_endpoint_policy"
        description = "Policy for tf role manaaging vpc endpoints"
        policy      = file("${path.root}/user_policies/tf_mosar_vpc_endpoint_policy.json")
      }
    }
  }
}

