# root/locals.tf

locals {
  users = {
    tf-mosar-rds = {
      path = "/terraform/"
      name = "tf_mosar_rds_${var.environment}"
      policy = {
        name        = "tf_mosar_rds_policy_${var.environment}"
        description = "Policy for tf role managing the rds cluster"
        policy      = file("${path.root}/user_policies/tf_mosar_rds_policy.json")
      }
    }
    tf-mosar-certificates = {
      path = "/terraform/"
      name = "tf_mosar_certificates_${var.environment}"
      policy = {
        name        = "tf_mosar_certificates_policy_${var.environment}"
        description = "Policy for tf role managing the certificates"
        policy      = file("${path.root}/user_policies/tf_mosar_certificates_policy.json")
      }
    }
    tf-mosar-feature-flags = {
      path = "/terraform/"
      name = "tf_mosar_feature_flags-${var.environment}"
      policy = {
        name        = "tf_mosar_feature_flags_policy_${var.environment}"
        description = "Policy for tf role managing the feature flags service (in ECS)"
        policy      = file("${path.root}/user_policies/tf_mosar_feature_flags_policy.json")
      }
    }
    tf-mosar-key-pairs = {
      path = "/terraform/"
      name = "tf_mosar_key_pairs-${var.environment}"
      policy = {
        name        = "tf_mosar_key_pairs_policy_${var.environment}"
        description = "Policy for tf role managing the key-pairs"
        policy      = file("${path.root}/user_policies/tf_mosar_key_pair_policy.json")
      }
    }
    tf-mosar-jump-servers = {
      path = "/terraform/"
      name = "tf_mosar_jump_servers-${var.environment}"
      policy = {
        name        = "tf_mosar_jump_servers_policy_${var.environment}"
        description = "Policy for tf role managing the jump servers"
        policy      = file("${path.root}/user_policies/tf_mosar_jump_servers_policy.json")
      }
    }
    tf-mosar-nat-instance = {
      path = "/terraform/"
      name = "tf_mosar_nat_instance-${var.environment}"
      policy = {
        name        = "tf_mosar_nat_instance_policy_${var.environment}"
        description = "Policy for tf role managing the nat instance"
        policy      = file("${path.root}/user_policies/tf_mosar_nat_instance_policy.json")
      }
    }
    tf-mosar-vpc = {
      path = "/terraform/"
      name = "tf_mosar_vpc-${var.environment}"
      policy = {
        name        = "tf_mosar_vpc_policy_${var.environment}"
        description = "Policy for tf role managing the vpc resources"
        policy      = file("${path.root}/user_policies/tf_mosar_vpc_policy.json")
      }
    }
    tf-mosar-ecs-cluster = {
      path = "/terraform/"
      name = "tf_mosar_ecs_cluster-${var.environment}"
      policy = {
        name        = "tf_mosar_ecs_cluster_policy_${var.environment}"
        description = "Policy for tf role managing the ecs cluster resources"
        policy      = file("${path.root}/user_policies/tf_mosar_ecs_cluster_policy.json")
      }
    }
    tf-ecr = {
      path = "/terraform/"
      name = "tf_ecr-${var.environment}"
      policy = {
        name        = "tf_ecr_policy_${var.environment}"
        description = "Policy for tf role managing ecr resources"
        policy      = file("${path.root}/user_policies/tf_ecr_policy.json")
      }
    }
  }
}

