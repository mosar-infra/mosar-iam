# root/main.tf

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.1.2"
  users       = local.users
  environment = var.environment
  managed_by  = "iam"
}

